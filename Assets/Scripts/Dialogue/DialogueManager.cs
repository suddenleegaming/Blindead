using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;

public class DialogueManager : MonoBehaviour
{
    private static DialogueManager instance;
    [Header("DialogueUI")]
    [SerializeField]
    private GameObject _dialoguePanel;
    [SerializeField]
    private TextMeshProUGUI _dialogueText;

    private Story _currentStory;
    public bool _dialogueIsPlaying {get; private set;}

    private void Awake()
     {
        if(instance!=null)
        {
            Debug.Log($"Found More than one Instance in Scene ");
        }
        instance=this;
    }
    private void Start() 
    {
        _dialogueIsPlaying=false;
        _dialoguePanel.SetActive(false);
    }
    private void Update() {
        if(!_dialogueIsPlaying)
        {
            return;
        }
        if(InputManager.GetInstance().GetSubmitPressed())
        {
            ContinueStory();
        }
    }
    public void EnterDialogueMode (TextAsset inkJSON) 
    {
        _currentStory= new Story(inkJSON.text);
        _dialogueIsPlaying=true;
        _dialoguePanel.SetActive(true);
        ContinueStory();

    }
    private void ExitDialogueMode()
    {
        _dialogueIsPlaying=false;
        _dialoguePanel.SetActive(false);
        _dialogueText.text="";
    }
    private void ContinueStory () {

        if(_currentStory.canContinue)
        {
            _dialogueText.text=_currentStory.Continue();
        }
        else
        {
            ExitDialogueMode();
        }
    }


    public static DialogueManager GetInstance()
    {
        return instance;
    }
}
